# Proyecto control clientes

## 1. Creamos el proyecto

```bash
ng new control-clientes
```

- Damos a la opcion de app-routing

## 2. Nos dirijimos a el proyecto

```bash
cd control-clientes
```

## 3. Agregamos **bootstrap** **jquery** y **popper.js**

```bash
npm i bootstrap jquery popper.js --save
```

## 4. Configuramos ahora el `angular.json` para poder usar el boostrap

```javascript
"styles": [
    "src/styles.css",
    "node_modules/bootstrap/dist/css/bootstrap.min.css"
],
"scripts": [
    "node_modules/jquery/dist/jquery.slim.min.js",
    "node_modules/popper.js/dist/umd/popper.min.js",
    "node_modules/bootstrap/dist/js/bootstrap.min.js"
]
```

## 5. Nos dirijimos a `src/index.html` y agregamos el fontAwesome

```HTML
<base href="/">
<script src="https://kit.fontawesome.com/9b4f4b1fe5.js" crossorigin="anonymous"></script>
```

## 6. Creamos los componentes

### cabecero

```bash
ng g c componentes/cabecero
```

### tablero

```bash
ng g c componentes/tablero
```

### clientes

```bash
ng g c componentes/clientes
```

### login

```bash
ng g c componentes/login
```

### registro

```bash
ng g c componentes/registro
```

### configuracion

```bash
ng g c componentes/configuracion
```

### no-encontrado

```bash
ng g c componentes/no-encontrado
```

### pie-pagina

```bash
ng g c componentes/pie-pagina
```

## 7. Instalamos firebase para firestore

```bash
npm install firebase @angular/fire --save
```

## 8. Para enviar mensajes desde la aplicacion con determinadas acciones

```bash
npm install angular2-flash-messages --save
```

## 9. configurar el `packege.json` para el prettier

```javascript
"prettier": {
    "trailingComma": "es5",
    "singleQuote": true
},
```

## 10. Creacion de las rutas en `src\app\app-routing.module.ts`

## 11. Usaremos **Compodoc** para la documentacion

```
npm install --save-dev @compodoc/compodoc
```

## 12. En `packege.json` configuramos añadimos em scripts

```javascript
"compodoc": "npx compodoc -p src/tsconfig.app.json -s -w"
```

## 13. Para generar el compodoc cree `tsconfig.app.json` en el src luego generalo y corralo

```bash
npm run compodoc
```

## 14. Configuramos `environment.prod.ts` y `environment.ts`

```javascript
firestore: {
    apiKey: 'AIzaSyA06V9JJ1dP-Ka4R25wOooN_DjYfEy0Zx4',
    authDomain: 'control-clientes-5bd07.firebaseapp.com',
    databaseURL: 'https://control-clientes-5bd07.firebaseio.com',
    projectId: 'control-clientes-5bd07',
    storageBucket: 'control-clientes-5bd07.appspot.com',
    messagingSenderId: '1071982077983',
    appId: '1:1071982077983:web:5d738598e3715d8fb28d2a',
  }
```

## 15. En `app.module.ts` importamos

```javascript
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import {
  AngularFirestoreModule,
  AngularFirestore,
} from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { FormsModule } from '@angular/forms';
```

## 15. En `app.module.ts` en **import[ ]**

```javascript
AngularFireModule.initializeApp(environment.firestore, 'control-clientes'),
AngularFirestoreModule,
AngularFireAuthModule,
FormsModule,
FlashMessagesModule.forRoot(),
```

## 16. Creamos el servicio cliente

```bash
ng g s servicios/cliente
```

## 17. Creamos el modelo cliente

## 18. Creamos el servicio login

```bash
ng g s servicios/login
```

## 19. Creamos el guardian auth

```bash
ng g g guardianes/auth
```

## 20. Creamos el servicio configuracion

```bash
ng g s servicios/configuracion
```

## 21. Creamos el guardian configiracion

```bash
ng g g guardianes/configuracion
```

## 22. Desplegar un app en firebase hosting
```bash
npm install -g firebase-tools
firebase login --reauth
firebase init
```
- Are you ready to proceed? Yes
- Firestore and Hosting
- Please select an option: Use an existing project
- Select a default Firebase project for this directory: control-clientes-5bd07 (control-clientes)
- What file should be used for Firestore Rules? firestore.rules
- What file should be used for Firestore indexes? firestore.indexes.json
- What do you want to use as your public directory? dist/control-clientes
- Configure as a single-page app (rewrite all urls to /index.html)? Yes

## 23. Hacemos el build de la app
```bash
ng build --prod --aot
```

## 24. Deployamos
```bash
firebase deploy
```

## 25. Vamos a nuestro hosting
```bash
firebase open hosting:site
```