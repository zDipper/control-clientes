import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FlashMessagesService } from 'angular2-flash-messages';
import { LoginService } from 'src/app/servicios/login.service';

/**
 * Componente de registro de clientes
 */
@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  /**
   * atributo email
   */
  email: string;
  /**
   * atributo password
   */
  password: string;
  /**
   * constructor de el componente de registro
   */
  constructor(
    private router: Router,
    private flashMessages: FlashMessagesService,
    private loginService: LoginService
  ) {}
  /**
   * No permitimos que se muestre si es que ya esta logueado
   */
  ngOnInit(): void {
    this.loginService.getAuth().subscribe((auth) => {
      if (auth) {
        this.router.navigate(['/']);
      }
    });
  }
  /**
   * tomamos los datos para el registro
   */
  registro() {
    this.loginService
      .registrarse(this.email, this.password)
      .then((resp) => {
        this.router.navigate(['/']);
      })
      .catch((error) => {
        this.flashMessages.show(error.message, {
          cssClass: 'alert-danger',
          timeout: 4000,
        });
      });
  }
}
