import { Component, OnInit } from '@angular/core';
import { FlashMessagesService } from 'angular2-flash-messages';
import { Router, ActivatedRoute } from '@angular/router';

import { Cliente } from 'src/app/modelo/cliente.model';
import { ClienteService } from 'src/app/servicios/cliente.service';
/**
 * En este componente editaremos y eliminaremos un cliente
 */
@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css'],
})
export class EditarClienteComponent implements OnInit {
  /**
   * Creamos un cliente y lo inicializamos de esta forma
   * @example
   * cliente: Cliente = {
   *  nombre: '',
   *  apellido: '',
   *  email: '',
   *  saldo: 0,
   * };
   */
  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0,
  };
  /**
   * el id que obtendremos del params
   */
  id: string;

  /**
   * aca inyectamos el servicio
   */
  constructor(
    private clienteServicio: ClienteService,
    private flashMessages: FlashMessagesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}
  /**
   * obtenemos el id y traemos los datos del cliente
   */
  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.clienteServicio.getCliente(this.id).subscribe((cliente) => {
      this.cliente = cliente;
    });
  }
  /**
   * guardaremos el cliente editado
   */
  guardar({ value, valid }: { value: Cliente; valid: boolean }) {
    if (!valid) {
      this.flashMessages.show('Por favor llena el formulario correctamente', {
        cssClas: 'alert-danger',
        timeout: 4000,
      });
    } else {
      value.id = this.id;
      // modificar al cliente
      this.clienteServicio.modificarCliente(value);
      this.router.navigate(['/']);
    }
  }
  /**
   * eliminaremos al cliente
   */
  eliminar() {
    if (confirm('seguro que desea eliminar al cliente')) {
      this.clienteServicio.eliminarCliente(this.cliente);
      this.router.navigate(['/']);
    }
  }
}
