import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/servicios/login.service';
import { Router } from '@angular/router';
import { ConfiguracionService } from 'src/app/servicios/configuracion.service';
/**
 * Componente de cabecero
 */
@Component({
  selector: 'app-cabecero',
  templateUrl: './cabecero.component.html',
  styleUrls: ['./cabecero.component.css'],
})
export class CabeceroComponent implements OnInit {
  /**
   * Para saber si el usuario esta logeado
   */
  isLoggedIn: boolean;
  /**
   * Aqui guardaremos el email
   */
  loggerInUser: string;
  /**
   * atributo para permitir el registro
   */
  permitirRegistro: boolean;
  /**
   * Constructor del componente
   */
  constructor(
    private loginService: LoginService,
    private router: Router,
    private configuracionService: ConfiguracionService
  ) {}
  /**
   * Obtenemos si un usuario esta logeado
   */
  ngOnInit(): void {
    this.loginService.getAuth().subscribe((auth) => {
      if (auth) {
        this.isLoggedIn = true;
        this.loggerInUser = auth.email;
      } else {
        this.isLoggedIn = false;
      }
    });
    this.configuracionService.getConfiguracion().subscribe((configuraciom) => {
      this.permitirRegistro = configuraciom.permitirRegistro;
    });
  }
  /**
   * funcionalidad del boton de logout
   */
  logout() {
    this.loginService.logout();
    this.isLoggedIn = false;
    this.router.navigate(['/login']);
    window.location.reload();
  }
}
