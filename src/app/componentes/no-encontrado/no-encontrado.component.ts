import { Component, OnInit } from '@angular/core';
/**
 * Componente de no encontrado
 */
@Component({
  selector: 'app-no-encontrado',
  templateUrl: './no-encontrado.component.html',
  styleUrls: ['./no-encontrado.component.css'],
})
export class NoEncontradoComponent implements OnInit {
  /**
   * constructor del componente de no encontrados
   */
  constructor() {}
  /**
   * @ignore
   */
  ngOnInit(): void {}
}
