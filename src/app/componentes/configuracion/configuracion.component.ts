import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionService } from 'src/app/servicios/configuracion.service';
import { Configuracion } from '../../modelo/configuracion.model';
/**
 * Componenete de la configuracion
 */
@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.component.html',
  styleUrls: ['./configuracion.component.css'],
})
export class ConfiguracionComponent implements OnInit {
  /**
   * inicializamos perimitir registro el false
   */
  permitirRegistro = false;
  /**
   * constructor del servicio de configuracion
   */
  constructor(
    private router: Router,
    private configuracionService: ConfiguracionService
  ) {}
  /**
   * inicializamos el permitir registro
   */
  ngOnInit(): void {
    this.configuracionService.getConfiguracion().subscribe((configuracion) => {
      this.permitirRegistro = configuracion.permitirRegistro;
    });
  }
  /**
   * guardamos la modificacion del permitir registro
   */
  guardar() {
    this.configuracionService.modificarConfiguracion({
      permitirRegistro: this.permitirRegistro,
    });
    this.router.navigate(['/']);
  }
}
