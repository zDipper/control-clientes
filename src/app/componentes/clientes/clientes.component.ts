import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { ClienteService } from 'src/app/servicios/cliente.service';
import { Cliente } from 'src/app/modelo/cliente.model';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';

/**
 * Componente Clientes donde tratamos la data del cliente
 */
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css'],
})
export class ClientesComponent implements OnInit {
  /**
   * Inicializamos clientes en un arreglo vacio
   */
  clientes: Cliente[];
  /**
   * Creamos un cliente y lo inicializamos de esta forma
   */
  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0,
  };
  /**
   * Para controlar el Formulario
   */
  @ViewChild('clienteForm') clienteForm: NgForm;
  /**
   * Para usar la funcion de salir del modal
   */
  @ViewChild('botonCerrar') botonCerrar: ElementRef;

  /**
   * aca inyectamos el servicio
   */
  constructor(
    private clienteServicio: ClienteService,
    private flashMessages: FlashMessagesService
  ) {}

  /**
   * Nos suscribimos el getClientes() del clienteService
   */
  ngOnInit(): void {
    this.clienteServicio.getClientes().subscribe((clientes) => {
      this.clientes = clientes;
    });
  }

  /**
   * Nos retorna el saldo de todos los clientes
   * @return saldoTotal
   */
  getSaldoTotal() {
    let saldoTotal = 0;
    if (this.clientes) {
      this.clientes.forEach((cliente) => {
        saldoTotal += cliente.saldo;
      });
    }
    return saldoTotal;
  }
  /**
   * Metodo para captar los valores del formulario
   */
  agregar({ value, valid }: { value: Cliente; valid: boolean }) {
    if (!valid) {
      this.flashMessages.show('POR FAVOR LLENA EL FORMULARIO CORRECTAMENTE', {
        cssClass: 'alert-danger',
        timeout: 4000,
      });
    } else {
      this.clienteServicio.agregarCliente(value);
      this.clienteForm.resetForm();
      this.cerrarModal();
    }
  }
  /**
   * metodo para cerrar el modal
   */
  private cerrarModal() {
    this.botonCerrar.nativeElement.click();
  }
}
