/**
 * modelo de configuracion
 */
export interface Configuracion {
  /**
   * atributo boolean de perimtir registro
   */
  permitirRegistro?: boolean;
}
