/**
 * Modelo del cliente
 */
export interface Cliente {
  /**
   * id del cliente
   */
  id?: string;
  /**
   * nombre del cliente
   */
  nombre?: string;
  /**
   * apellido del cliente
   */
  apellido?: string;
  /**
   * email del cliente
   */
  email?: string;
  /**
   * saldo del cliente
   */
  saldo?: number;
}
