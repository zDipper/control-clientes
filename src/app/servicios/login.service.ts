import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

/**
 * Servicio del login
 */
@Injectable({
  providedIn: 'root',
})
export class LoginService {
  /**
   * Constructor del Login Sevice
   */
  constructor(private authService: AngularFireAuth) {}
  /**
   * Recibimos el email y el password para la autenticacion
   */
  login(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.authService.signInWithEmailAndPassword(email, password).then(
        (datos) => resolve(datos),
        (error) => reject(error)
      );
    });
  }
  /**
   * Obtenemos el usuario autenticado
   */
  getAuth() {
    return this.authService.authState.pipe(map((auth) => auth));
  }
  /**
   * Borramos la autenticacion
   */
  logout() {
    this.authService.signOut();
  }
  /**
   * Registro de un nuevo usuario
   */
  registrarse(email: string, password: string) {
    return new Promise((resolve, reject) => {
      this.authService.createUserWithEmailAndPassword(email, password).then(
        (datos) => resolve(datos),
        (error) => reject(error)
      );
    });
  }
}
