import { Injectable } from '@angular/core';
import {
  AngularFirestoreCollection,
  AngularFirestoreDocument,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Cliente } from '../modelo/cliente.model';

/**
 * Servicio para el cliente
 */
@Injectable({
  providedIn: 'root',
})
export class ClienteService {
  /**
   * Es un AngularFirestoreCollection con formato de Cliente
   */
  clientesColeccion: AngularFirestoreCollection<Cliente>;
  /**
   * Es un AngularFirestoreDocument con formato de Cliente
   */
  clienteDoc: AngularFirestoreDocument<Cliente>;
  /**
   * Es un Observable de un arreglo de Clientes
   */
  clientes: Observable<Cliente[]>;
  /**
   * Es un Observable de Cliente
   */
  cliente: Observable<Cliente>;
  /**
   * Constructor de el servicio
   */
  constructor(private db: AngularFirestore) {
    this.clientesColeccion = db.collection('clientes', (ref) =>
      ref.orderBy('nombre', 'asc')
    );
  }
  /**
   * recupera los clientes
   * @returns Observable<Cliente[]>
   */
  getClientes(): Observable<Cliente[]> {
    return this.clientesColeccion.snapshotChanges().pipe(
      map((cambios) => {
        return cambios.map((accion) => {
          const datos = accion.payload.doc.data() as Cliente;
          datos.id = accion.payload.doc.id;
          return datos;
        });
      })
    );
  }
  /**
   * metodo para agregar un cliente
   */
  agregarCliente(cliente: Cliente) {
    this.clientesColeccion.add(cliente);
  }
  /**
   * Obtenermos un id y regresamos un cliente en especifico
   */
  getCliente(id: string) {
    this.clienteDoc = this.db.doc<Cliente>(`clientes/${id}`);
    return this.clienteDoc.snapshotChanges().pipe(
      map((accion) => {
        if (accion.payload.exists === false) {
          return null;
        } else {
          const datos = accion.payload.data() as Cliente;
          datos.id = accion.payload.id;
          return datos;
        }
      })
    );
  }
  /**
   * modificamos un cliente por su id
   */
  modificarCliente(cliente: Cliente) {
    this.clienteDoc = this.db.doc(`clientes/${cliente.id}`);
    this.clienteDoc.update(cliente);
  }
  /**
   * eliminamos un cliente
   */
  eliminarCliente(cliente: Cliente) {
    this.clienteDoc = this.db.doc(`clientes/${cliente.id}`);
    this.clienteDoc.delete();
  }
}
