import { Injectable } from '@angular/core';
import {
  AngularFirestoreDocument,
  AngularFirestore,
} from '@angular/fire/firestore';
import { Configuracion } from '../modelo/configuracion.model';
import { Observable } from 'rxjs';
/**
 * Servicio de configuracion
 */
@Injectable({
  providedIn: 'root',
})
export class ConfiguracionService {
  /**
   * documento de condiguracion
   */
  configuracionDoc: AngularFirestoreDocument<Configuracion>;
  /**
   * observable del tipo configuracion
   */
  configuracion: Observable<Configuracion>;
  /**
   * id del documento
   */
  id = 1;
  /**
   * constructor del servicio configuracion
   */
  constructor(private db: AngularFirestore) {}
  /**
   * obtenemos el valor de configuracion de firestore y guardamos el valor en configuracion
   */
  getConfiguracion(): Observable<Configuracion> {
    this.configuracionDoc = this.db.doc<Configuracion>(
      `configuracion/${this.id}`
    );
    this.configuracion = this.configuracionDoc.valueChanges();
    return this.configuracion;
  }
  /**
   * modificaion de la configuracion
   */
  modificarConfiguracion(configuracion: Configuracion) {
    this.configuracionDoc = this.db.doc<Configuracion>(
      `configuracion/${this.id}`
    );
    this.configuracionDoc.update(configuracion);
  }
}
