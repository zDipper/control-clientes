import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';

import { ConfiguracionService } from '../servicios/configuracion.service';
/**
 * guardian para el registro
 */
@Injectable({
  providedIn: 'root',
})
export class ConfiguracionGuard implements CanActivate {
  /**
   * constructor de el guardian de configuracion
   */
  constructor(
    private router: Router,
    private afAuth: AngularFireAuth,
    private configuracionServicio: ConfiguracionService
  ) {}
  /**
   * Devuelve un observable de tipo configuracion
   */
  canActivate(): Observable<boolean> {
    return this.configuracionServicio.getConfiguracion().pipe(
      map((configuracion) => {
        if (configuracion.permitirRegistro) {
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      })
    );
  }
}
