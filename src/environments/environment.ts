// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore: {
    apiKey: 'AIzaSyA06V9JJ1dP-Ka4R25wOooN_DjYfEy0Zx4',
    authDomain: 'control-clientes-5bd07.firebaseapp.com',
    databaseURL: 'https://control-clientes-5bd07.firebaseio.com',
    projectId: 'control-clientes-5bd07',
    storageBucket: 'control-clientes-5bd07.appspot.com',
    messagingSenderId: '1071982077983',
    appId: '1:1071982077983:web:5d738598e3715d8fb28d2a',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
